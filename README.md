## docker-pets

### Docker Pets from https://github.com/dockersamples/docker-pets, added .gitlab-ci.yml to be built using Gitlab CI.

# Desafio da Agility


### Como executar:
Basta criar uma tag nova neste repositório (https://gitlab.com/darthcabs/docker-pets)

*Pedi para a Priscila me passar o usuário do avaliador no Gitlab, para que eu possa adicioná-lo ao repositório e ele possa criar tags e visualizar os pipelines*.

### Fluxo: 
#### Gitlab CI: 
* Constrói uma imagem Docker da página web Docker Pets; 
* Envia para o registry associado a esse mesmo repositório (registry.gitlab.com/darthcabs/docker-pets) com a tag do hash curto do commit e com a tag _latest_;
* Executa a receita do Terraform localizada em https://gitlab.com/darthcabs/terraform-ec2. 

#### Terraform:
Cria uma instância em uma conta AWS especificada (detalhes abaixo);
* Associa um _Security Group_ a essa instância para liberar as portas 5000 e 7000, além da porta 22 (para conexão SSH);
* Instala o Docker nessa instância; 
* Executa a imagem Docker contruída acima.


### Detalhamento:
Para que o Gitlab CI seja executado em um repositório do Gitlab, basta que ele contenha em sua raíz o arquivo `.gitlab-ci.yml`, que contém instruções sobre como esse processo deve ser executado.

Esse processo está dividido em etapas, chamadas de _stages_ que são executadas em uma imagem Docker, localizada no repositório https://gitlab.com/darthcabs/docker-ssh.

Esse _pipeline_ de CI está configurado para ser disparado toda vez que uma _tag_ for criada no repositório. Essa é uma boa prática de CI/CD, já que as tags são associadas a um commit em específico, e não a uma branch inteira (que tem seu conteúdo alterado com frequência).

Além de ser disparado na criação de tags, esse pipeline utiliza parâmetros sensíveis ao usuário, que são recebidos na forma de variáveis de ambiente e arquivos de _secrets_, como por exemplo a chave SSH usada pelo Terraform para se conectar à instância.

Esses parâmetros são configurados no repositório via UI do Gitlab, em _Settings -> CI/CD_:

<img src="parameters.png">Parâmetros</img>

São eles:
* `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` e `AWS_DEFAULT_REGION`: Usados pelo Terraform para fazer _deploy_ da instância;
* `GITLAB_TOKEN`: Usado para enviar a imagem Docker recém construída para o registry do Gitlab. Para tal, eu criei anteriormente um Access Token para o meu usuário do Gitlab, que expira em 15 dias;
* O arquivo `ec2-key`: Chave SSH (.pem) para que o Terraform se conecte à instância. Esse arquivo foi gerado pela AWS no momento da criação da _Key Pair_ via console da AWS (manualmente). Em um ambiente de produção as chaves SSH são muito bem salvas utilizando-se uma ferramenta de cofre, como por exemplo o Vault da Hashicorp. Por esse motivo preferi criar a Key Pair de forma manual anteriormente e passá-la ao pipeline via arquivo secret.

### Notas especiais:
- Ao início do desafio, minha ideia era utilizar o Jenkins, que é a ferramenta de CI/CD com a qual estou mais habituado. No entanto é bastante complicado deixar o Jenkins num estado funcional, com webhook do Gitlab (ou mesmo do Github) configurado de modo completamente automático, sem precisar de interação manual. Por esse motivo acabei optando por utilizar o Gitlab CI. Por mais que eu nunca o tivesse usado antes, fez mais sentido aprender essa ferramenta nova que já trazia pronta boa parte das interações com o repositório.
- Decidi remover do pipeline o _stage_ de testes. É possível perceber pelos meus commits que tentei de várias formas bater no serviço na porta 5000 configurando um timeout de 60 segundos, mas estava tendo muitos problemas com o _Runner_ do Gitlab CI, e por isso, com muita relutância acabei por remover esse stage de última hora. Com um pouco mais de tempo e de aprendizado, tenho certeza que é plenamente possível implementar esse passo novamente.